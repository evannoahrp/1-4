public class MainObat {
    public static void main(String[] args) {
        Obat obj = new Obat();
        obj.setId(1);
        obj.setNama("Obat Batuk");
        obj.setHarga(5000);
        System.out.println(obj.getId() + ", " + obj.getNama() + ", " + obj.getHarga());
        Obat obj2 = new Obat(2, "Obat Panu", 5000);
        System.out.println(obj2.getId() + ", " + obj2.getNama() + ", " + obj2.getHarga());
    }
}
